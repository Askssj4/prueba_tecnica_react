import React, { useState } from 'react';
import {
    Nav,
    NavItem,
    NavLink,
    TabContent,
    TabPane,
    Row,
    Col,
    Card,
    CardTitle,
    CardText,
    Button,
} from 'reactstrap';
const MenuTurismo = props => {

    const [pestañaActiva, setPestañaActiva] = useState(1);

    const cambiarPestaña=(pestaña)=>{
        setPestañaActiva(pestaña)
        props.opcion_menu(pestaña)
    }

    return (
        <div>
            <Nav tabs>
                <NavItem>
                    <NavLink
                         className={pestañaActiva==1?'active':''}
                        onClick={()=>{cambiarPestaña(1)}}
                    >
                        Inicio
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink
                        className={pestañaActiva==2?'active':''}
                        onClick={()=>{cambiarPestaña(2)}}
                    >
                        Paquetes
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink
                        className={pestañaActiva==3?'active':''}
                        onClick={()=>{cambiarPestaña(3)}}
                    >
                        Nosotros
                    </NavLink>
                </NavItem>
            </Nav>
            <TabContent activeTab={pestañaActiva}>
                <TabPane tabId={1}>
                    <Row>
                        <Col sm="12">
                            
                        </Col>
                    </Row>
                </TabPane>
                <TabPane tabId={2}>
                    <Row>
                        <Col>
                        
                        </Col>
                    </Row>
                </TabPane>
            </TabContent>
            
        </div>
    );
}

export default MenuTurismo;