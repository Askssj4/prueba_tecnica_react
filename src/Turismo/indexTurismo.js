import { useState, useEffect, Fragment } from "react";
import { Container, Row, Col } from 'reactstrap'
import MenuTurismo from "./MenuTurismo";
import InicioTurismo from "./inicioTurismo";
import PaquetesTurismo from "./paquetesTurismo";

const IndexTurismo = props => {

    const [opcionMenuSeleccionada, setOpcionMenuSeleccionada] = useState(1)

    const opcionMenu=opcion=>{
        console.log("OPCION", opcion);
        setOpcionMenuSeleccionada(opcion)
    }

    return (
        <div className="page-content">
            <Container fluid={false}>
                <Row>
                    <Col>
                        <img
                            style={{ height: '20vh', width: '50vh' }}
                            src={'https://www.elsv.info/wp-content/uploads/2016/10/El-Salvador-1024x548.png'}
                        />
                    </Col>
                    <Col style={{alignSelf:"flex-start"}}>
                        <MenuTurismo 
                        opcion_menu={(opcion)=>{opcionMenu(opcion)}}
                        />
                    </Col>
                </Row>
                <hr/>
                <br/>
                <Row>
                    <Col>
                        {opcionMenuSeleccionada==1?<InicioTurismo />:opcionMenuSeleccionada==2?<PaquetesTurismo/>:""}
                    </Col>
                </Row>
                <br/>
            </Container>
        </div>
    )
}

export default IndexTurismo

