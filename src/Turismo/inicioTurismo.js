import { useState, useEffect, Fragment } from "react";
import { Container, Row, Col, Label, Input, Button } from 'reactstrap'
import MenuTurismo from "./MenuTurismo";
import { FaMapMarker } from 'react-icons/fa';
import { IconContext } from "react-icons";
import request from "superagent";
import Swal from 'sweetalert2'

const InicioTurismo = props => {

    useEffect(()=>{
        _obtenerSitios()
        _pruebaPokeApi()
    },[])


    //Se hace esta otra función como comprobación que el request de superagent si consume la pokeapi a diferencia de la de google api places
    const _pruebaPokeApi=async()=>{
        try {
            let respuesta_sitios = await request.get('https://pokeapi.co/api/v2/pokemon')
            .set('Access-Control-Allow-Origin', '*')

            console.log("PRUEBA POKEAPI",respuesta_sitios);
        } catch (error) {
            
        }
    }

    const _obtenerSitios=async()=>{
        try {   
            let respuesta_sitios = await request.get('https://maps.googleapis.com/maps/api/place/textsearch/json?')
            .query({query: 'parks in El Salvador', key: "AIzaSyBLy9dNxKHhMATbTrasUbYHLNIcB_uuVZI"})

            console.log("SITIOS",respuesta_sitios);
            
        } catch (error) {
            console.log(error);
            Swal.fire({
                icon: 'error',
                title: 'Error al obtener los sitios turisticos',
                text: 'No se han podido recuperar los sitios tiristicos',
            })
        }

    }

    return (
        <div className="page-content">
            <Container fluid={false}>
                <Row>
                    <Col>
                        <Row>
                            <Col>
                                <h2 style={{ color: "green" }}>
                                    <b>Descubre aquí los lugares más increibles</b>
                                </h2>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={1}>

                                <IconContext.Provider value={{ color: "red", className: "global-class-name" }}>
                                    <div>
                                        <FaMapMarker />
                                    </div>
                                </IconContext.Provider>
                            </Col>
                            <Col>
                                <Input
                                    id="busquedaLugarezs"
                                    name="busqueda"
                                    placeholder="Encuentralo aquí"
                                />
                            </Col>
                            <Col style={{alignSelf:"center"}}>
                                <Button size="sm" color="primary" onClick={() => { }}>
                                    Buscar
                                </Button>
                            </Col>
                        </Row>
                    </Col>
                    <Col>
                        <img
                            style={{ height: '50vh', width: '50vh' }}
                            src={'https://w7.pngwing.com/pngs/521/268/png-transparent-travel-tourism-baggage-travel-text-logo-travel-agent.png'}
                        />
                    </Col>
                </Row>
                <hr/>
                <br/>
                <Row>
                    <Col md={2}>
                        <h5>
                            <b>Paquetes</b>
                        </h5>
                    </Col>
                    <Col>
                        <hr/>
                    </Col>
                </Row>

                <hr/>
                <br/>
                <Row>
                    <Col md={3}>
                        <h5>
                            <b>Publicidad de comercios</b>
                        </h5>
                    </Col>
                    <Col>
                        <hr/>
                    </Col>
                </Row>
            </Container >
        </div >
    )
}

export default InicioTurismo


